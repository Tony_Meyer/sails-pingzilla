/**
 * Bootstrap
 *
 * An asynchronous boostrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#documentation
 */

module.exports.bootstrap = function (cb) {

  
//   var dummyUsers = [
// 	{
// 		"name": "Bevis",
// 		"email": "eu@Nam.net"
// 	},
// 	{
// 		"name": "Chandler",
// 		"email": "quam.Curabitur.vel@sit.org"
// 	},
// 	{
// 		"name": "Dai",
// 		"email": "felis.purus.ac@Loremipsumdolor.ca"
// 	},
// 	{
// 		"name": "Clare",
// 		"email": "Donec.felis.orci@orci.ca"
// 	},
// 	{
// 		"name": "Karina",
// 		"email": "at@musAeneaneget.net"
// 	},
// 	{
// 		"name": "Kylynn",
// 		"email": "nibh.lacinia.orci@mattisvelit.org"
// 	},
// 	{
// 		"name": "Russell",
// 		"email": "ornare.tortor.at@Duisatlacus.com"
// 	},
// 	{
// 		"name": "Uriah",
// 		"email": "a.scelerisque.sed@velarcuCurabitur.com"
// 	},
// 	{
// 		"name": "Chancellor",
// 		"email": "sollicitudin.commodo@quisarcu.edu"
// 	},
// 	{
// 		"name": "Doris",
// 		"email": "dui@iaculisaliquetdiam.org"
// 	},
// 	{
// 		"name": "Felix",
// 		"email": "gravida@diameudolor.ca"
// 	},
// 	{
// 		"name": "Joan",
// 		"email": "dictum.augue@montesnasceturridiculus.edu"
// 	},
// 	{
// 		"name": "Vernon",
// 		"email": "Quisque.imperdiet.erat@Namconsequat.edu"
// 	},
// 	{
// 		"name": "Chastity",
// 		"email": "Maecenas@Innecorci.co.uk"
// 	},
// 	{
// 		"name": "Quin",
// 		"email": "justo.eu.arcu@vitaeodio.edu"
// 	},
// 	{
// 		"name": "Kai",
// 		"email": "convallis.ligula@parturientmontesnascetur.ca"
// 	},
// 	{
// 		"name": "Faith",
// 		"email": "Fusce.aliquam@dolorvitaedolor.ca"
// 	},
// 	{
// 		"name": "Logan",
// 		"email": "Integer.urna.Vivamus@ante.org"
// 	},
// 	{
// 		"name": "Emery",
// 		"email": "diam.Sed.diam@acsemut.net"
// 	},
// 	{
// 		"name": "Kane",
// 		"email": "Phasellus@Donecfelisorci.co.uk"
// 	},
// 	{
// 		"name": "Beatrice",
// 		"email": "mauris.ut.mi@dignissimtempor.com"
// 	},
// 	{
// 		"name": "Hall",
// 		"email": "tellus.Nunc@enimconsequat.edu"
// 	},
// 	{
// 		"name": "Courtney",
// 		"email": "molestie.pharetra@semsempererat.co.uk"
// 	},
// 	{
// 		"name": "Selma",
// 		"email": "enim.diam.vel@nislMaecenasmalesuada.co.uk"
// 	},
// 	{
// 		"name": "Perry",
// 		"email": "dolor@loremsemper.net"
// 	},
// 	{
// 		"name": "Boris",
// 		"email": "rhoncus.Donec.est@enimgravida.edu"
// 	},
// 	{
// 		"name": "Jayme",
// 		"email": "inceptos@quamdignissimpharetra.edu"
// 	},
// 	{
// 		"name": "Kelly",
// 		"email": "Proin.sed.turpis@diamvelarcu.ca"
// 	},
// 	{
// 		"name": "Cameron",
// 		"email": "dis.parturient@eu.org"
// 	},
// 	{
// 		"name": "Branden",
// 		"email": "Fusce.diam.nunc@ipsumSuspendisse.org"
// 	},
// 	{
// 		"name": "Kyle",
// 		"email": "ornare.Fusce@quamPellentesquehabitant.ca"
// 	},
// 	{
// 		"name": "Ciara",
// 		"email": "pellentesque@acturpis.co.uk"
// 	},
// 	{
// 		"name": "Carla",
// 		"email": "semper@turpisnecmauris.edu"
// 	},
// 	{
// 		"name": "Isabella",
// 		"email": "in@Integer.co.uk"
// 	},
// 	{
// 		"name": "Jenette",
// 		"email": "Sed.auctor.odio@sedleoCras.com"
// 	},
// 	{
// 		"name": "Natalie",
// 		"email": "dictum.cursus@metusInlorem.org"
// 	},
// 	{
// 		"name": "Darius",
// 		"email": "eget.mollis@turpis.com"
// 	},
// 	{
// 		"name": "Portia",
// 		"email": "auctor.velit@sedhendrerit.co.uk"
// 	},
// 	{
// 		"name": "Alyssa",
// 		"email": "enim.nisl.elementum@vitaeerat.org"
// 	},
// 	{
// 		"name": "Maris",
// 		"email": "urna.Nullam@Maurisquis.org"
// 	},
// 	{
// 		"name": "Nero",
// 		"email": "Donec.felis.orci@arcuCurabitur.com"
// 	},
// 	{
// 		"name": "Teagan",
// 		"email": "egestas.Aliquam.fringilla@etliberoProin.org"
// 	},
// 	{
// 		"name": "Dominique",
// 		"email": "vulputate.dui.nec@nisiaodio.edu"
// 	},
// 	{
// 		"name": "Denise",
// 		"email": "litora.torquent.per@nonummy.edu"
// 	},
// 	{
// 		"name": "Salvador",
// 		"email": "Nunc.commodo@odioapurus.net"
// 	},
// 	{
// 		"name": "Stone",
// 		"email": "ut.nisi.a@eget.edu"
// 	},
// 	{
// 		"name": "Melvin",
// 		"email": "nunc.interdum@interdumfeugiatSed.edu"
// 	},
// 	{
// 		"name": "Chloe",
// 		"email": "commodo@estmauris.ca"
// 	},
// 	{
// 		"name": "Abbot",
// 		"email": "velit@risusat.co.uk"
// 	},
// 	{
// 		"name": "Clementine",
// 		"email": "adipiscing.lobortis.risus@pharetraut.edu"
// 	},
// 	{
// 		"name": "Velma",
// 		"email": "metus.vitae@eunullaat.co.uk"
// 	},
// 	{
// 		"name": "Adena",
// 		"email": "Aliquam@posuere.edu"
// 	},
// 	{
// 		"name": "Camden",
// 		"email": "vehicula.Pellentesque.tincidunt@eros.com"
// 	},
// 	{
// 		"name": "Kamal",
// 		"email": "augue.id@risus.co.uk"
// 	},
// 	{
// 		"name": "Serina",
// 		"email": "neque.tellus@Praesenteu.net"
// 	},
// 	{
// 		"name": "Blaine",
// 		"email": "libero@venenatisamagna.com"
// 	},
// 	{
// 		"name": "Leonard",
// 		"email": "mus.Proin@commodotinciduntnibh.ca"
// 	},
// 	{
// 		"name": "Sebastian",
// 		"email": "Quisque.ornare@arcu.co.uk"
// 	},
// 	{
// 		"name": "Adele",
// 		"email": "penatibus.et.magnis@vestibulumMaurismagna.ca"
// 	},
// 	{
// 		"name": "Driscoll",
// 		"email": "ante.dictum@quamafelis.com"
// 	},
// 	{
// 		"name": "Idola",
// 		"email": "Ut.sagittis.lobortis@dictum.net"
// 	},
// 	{
// 		"name": "Cyrus",
// 		"email": "at@idenimCurabitur.co.uk"
// 	},
// 	{
// 		"name": "Zachery",
// 		"email": "nibh.vulputate.mauris@adipiscing.org"
// 	},
// 	{
// 		"name": "Chaney",
// 		"email": "Nulla@magnaDuisdignissim.org"
// 	},
// 	{
// 		"name": "Anjolie",
// 		"email": "tincidunt.nibh.Phasellus@diamSeddiam.org"
// 	},
// 	{
// 		"name": "Sylvia",
// 		"email": "lacus.Quisque@tincidunt.co.uk"
// 	},
// 	{
// 		"name": "Slade",
// 		"email": "dignissim.pharetra.Nam@vitaeeratvel.co.uk"
// 	},
// 	{
// 		"name": "Sylvester",
// 		"email": "id@nuncullamcorpereu.edu"
// 	},
// 	{
// 		"name": "Demetrius",
// 		"email": "Donec@tellus.edu"
// 	},
// 	{
// 		"name": "Brett",
// 		"email": "fringilla.Donec@Nullamvelit.org"
// 	},
// 	{
// 		"name": "Tana",
// 		"email": "Phasellus.dapibus.quam@Curabiturutodio.ca"
// 	},
// 	{
// 		"name": "Xandra",
// 		"email": "interdum.Nunc.sollicitudin@liberoest.edu"
// 	},
// 	{
// 		"name": "Zena",
// 		"email": "nec.mauris.blandit@sitamet.org"
// 	},
// 	{
// 		"name": "Nathan",
// 		"email": "ipsum.nunc@ipsum.ca"
// 	},
// 	{
// 		"name": "Maggy",
// 		"email": "ac.turpis.egestas@consequatauctor.net"
// 	},
// 	{
// 		"name": "Arthur",
// 		"email": "convallis@rutrumjustoPraesent.edu"
// 	},
// 	{
// 		"name": "Clark",
// 		"email": "magnis@rhoncus.net"
// 	},
// 	{
// 		"name": "Keelie",
// 		"email": "non@egestas.com"
// 	},
// 	{
// 		"name": "Sierra",
// 		"email": "molestie.arcu@necdiamDuis.ca"
// 	},
// 	{
// 		"name": "Callum",
// 		"email": "vehicula.Pellentesque@volutpat.co.uk"
// 	},
// 	{
// 		"name": "Rogan",
// 		"email": "urna.justo@euultrices.net"
// 	},
// 	{
// 		"name": "Hayden",
// 		"email": "sapien@laoreet.ca"
// 	},
// 	{
// 		"name": "Hope",
// 		"email": "aptent@nibhAliquam.net"
// 	},
// 	{
// 		"name": "Kirsten",
// 		"email": "arcu@leoCras.net"
// 	},
// 	{
// 		"name": "Nichole",
// 		"email": "montes@eratVivamusnisi.com"
// 	},
// 	{
// 		"name": "Slade",
// 		"email": "augue.ut@ornaresagittisfelis.com"
// 	},
// 	{
// 		"name": "Sebastian",
// 		"email": "magna@sitamet.ca"
// 	},
// 	{
// 		"name": "Althea",
// 		"email": "dictum.eu@porttitor.co.uk"
// 	},
// 	{
// 		"name": "Lenore",
// 		"email": "nec.tempus@dapibusgravidaAliquam.net"
// 	},
// 	{
// 		"name": "Aretha",
// 		"email": "at.risus.Nunc@Inscelerisquescelerisque.com"
// 	},
// 	{
// 		"name": "Fallon",
// 		"email": "nostra.per.inceptos@perconubianostra.com"
// 	},
// 	{
// 		"name": "Quynn",
// 		"email": "Aenean.eget.magna@eueros.org"
// 	},
// 	{
// 		"name": "Maya",
// 		"email": "velit@luctus.org"
// 	},
// 	{
// 		"name": "Jesse",
// 		"email": "mauris.sagittis.placerat@ategestas.edu"
// 	},
// 	{
// 		"name": "Germaine",
// 		"email": "vestibulum.massa@vitae.co.uk"
// 	},
// 	{
// 		"name": "Bertha",
// 		"email": "vitae@inceptoshymenaeos.edu"
// 	},
// 	{
// 		"name": "Hector",
// 		"email": "ligula@semperrutrum.co.uk"
// 	},
// 	{
// 		"name": "Elaine",
// 		"email": "feugiat@ipsumSuspendisse.ca"
// 	},
// 	{
// 		"name": "Carol",
// 		"email": "sit.amet.ultricies@at.co.uk"
// 	},
// 	{
// 		"name": "Heidi",
// 		"email": "et.euismod.et@semperNamtempor.co.uk"
// 	}
// ];

//   User.count().exec(function(err, count) {
//     if(err) {
//       sails.log.error('Already have data.');
//       return cb(err);
//     }
//     if(count > 0) return cb();

//     User.create(dummyUsers).exec(cb);
//   });

  // It's very important to trigger this callack method when you are finished 
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};