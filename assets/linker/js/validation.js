$(document).ready(function() {

	$('.signup-form').validate({
		rules: {

			name: {
				required: true
			},

			email: {
				required: true,
				email: true
			},

			password: {
				minlength: 6,
				required: true
			},

			confirmation: {
				minlength: 6,
				required: true,
				equalTo: "#inputPassword"
			}

		},
		success: function(element) {
			element
			.text('Good!').addClass('valid')
		}

	});


});